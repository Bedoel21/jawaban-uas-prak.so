# Base Image
From debian:bullseye-slim

#install
Run apt-get update && apt-get install -y git neovim netcat python3-pip

#copy web service filea
COPY emergency_numbers.py /home/nomor-darurat/emergency_numbers.py
COPY requirements.txt /home/nomor-darurat/requirements.txt

#Install Python dependencies
RUN pip3 install --no-cache-dir -r /home/nomor-darurat/requirements.txt

#set working directory
WORKDIR /home/nomor-darurat

#expose port
EXPOSE 25001

#start web service
CMD ["streamlit", "run", "--server.port=25001", "emergency_numbers.py"]