# 1. Soal lengkap: https://gitlab.com/marchgis/march-ed/2023/courses/if214009-praktikum-sistem-operasi/-/tree/main/business-pitch

# 2. Mampu mendemonstrasikan pembuatan Docker Image melalui Dockerfile, sebagai bentuk implementasi dari sistem operasi (kumpulkan url Gitlab dari Dockerfile dan file2 pendukungnya)
> terdapat beberapa file pendukung, diantaranya:
   
    - docker-compose.yml
    - Dockerfile
    - emergency_number.py
    - requirments.txt
> seperti pada gambar di bawah

![dok_so](dok_so/file.jpg)

# 3. Mampu mendemonstrasikan pembuatan web page / web service sederhana berdasarkan permasalahan dunia nyata yang dijalankan di dalam Docker Container (kumpulkan url Gitlab dari web page / web servicenya)
> berikut link web page yang saya buat:

>http://135.181.26.148:25001 #daftar-panggilan-darurat-di-indonesia

# 4. Mampu mendemonstrasikan penggunaan Docker Compose pada port tertentu sebagai bentuk kontainerisasi program dan sistem operasi yang menjadi dasar distributed computing (kumpulkan url Gitlab docker-compose.yml dan folder terkaitnya)
> terdapat beberapa file pendukung, diantaranya:
   
    - docker-compose.yml
    - Dockerfile
    - emergency_number.py
    - requirments.txt
> seperti pada gambar di bawah

![dok_so](dok_so/file.jpg)

# 5. Mampu menjelaskan project yang dibuat dalam bentuk file README.md

- **Deskripsi project**

![dok_so](dok_so/nomor_darurat.jpg)

**Program Nomor Darurat** adalah sebuah aplikasi sederhana yang bertujuan untuk memberikan informasi penting mengenai nomor darurat yang dapat dihubungi dalam situasi darurat di Indonesia. Tujuan utama dari program ini adalah memberikan akses cepat dan mudah kepada pengguna untuk mendapatkan nomor darurat yang relevan dan memungkinkan pengguna untuk mengambil langkah yang tepat dalam situasi darurat.

Ketika program dijalankan, sebuah antarmuka pengguna interaktif akan muncul dengan judul "Daftar Panggilan Darurat di Indonesia". Di antarmuka ini, pengguna akan diberikan daftar pilihan jenis panggilan darurat yang mencakup berbagai keadaan darurat, seperti kepolisian, pemadam kebakaran, ambulans, dan lainnya.

Pengguna dapat memilih jenis panggilan darurat yang relevan dengan keadaan yang mereka hadapi atau yang mereka butuhkan. Setelah memilih, pengguna dapat mengeklik tombol "Tampilkan nomor darurat" untuk melihat nomor telepon darurat yang terkait dengan jenis panggilan darurat yang dipilih.

Misalnya, jika pengguna memilih "Polisi" dan mengklik tombol, program akan menampilkan nomor darurat Polisi, yaitu "110". Informasi ini sangat penting dalam situasi darurat yang memerlukan bantuan dan intervensi dari pihak berwenang terkait.

Program Nomor Darurat memberikan keuntungan bagi pengguna dengan menyediakan akses cepat dan mudah terhadap nomor darurat yang diperlukan. Dengan antarmuka pengguna yang sederhana dan intuitif, pengguna dapat dengan mudah menemukan dan menghubungi nomor darurat yang sesuai dengan keadaan darurat yang dihadapi.

Melalui program ini, diharapkan pengguna dapat memperoleh informasi yang penting dan relevan dalam situasi darurat, yang dapat membantu meningkatkan keselamatan, keamanan, dan respons yang tepat dalam menghadapi keadaan darurat di Indonesia.

## Sistem operasi digunakan dalam proses containerization

Sistem operasi  berperan sebagai platform atau tempat untuk menjalankan aplikasi dan container. Bayangkan sistem operasi seperti sebuah rumah yang lebih besar yang menampung banyak rumah mini, sistem operasi menyediakan sumber daya seperti CPU, memori, dan jaringan untuk menjalankan semua container dan aplikasi di dalamnya.

Rumah besar itu atau sistem operasi  juga bertanggung jawab untuk menjalankan tugas-tugas penting lainnya, seperti mengelola file dan folder, menjalankan program, menyediakan layanan jaringan, dan menjaga keamanan sistem. Ini memastikan bahwa semua container dan aplikasi berjalan dengan lancar dan aman.

Jadi, sistem operasi dalam konteks containerisasi adalah sistem operasi host atau server di mana semua container dan aplikasi berjalan, dan yang menyediakan sumber daya serta layanan dasar untuk menjalankan semua itu.

## Cara containerization dapat membantu mempermudah pengembangan aplikasi

Dengan menggunakan container, pengembang dapat membuat aplikasi dengan lebih mudah. Mereka dapat mengemas semua yang diperlukan, seperti bahasa pemrograman dan alat bantu, ke dalam container tersebut. Ini membantu pengembang agar tidak perlu khawatir tentang bagaimana aplikasi akan berjalan di komputer lain yang mungkin memiliki pengaturan yang berbeda.

Container juga memungkinkan pengembang untuk membawa aplikasi ke mana pun. Mereka dapat menjalankannya di komputer lain dengan mudah. Hal ini sangat berguna jika pengembang ingin membagikan aplikasi  atau menjalankannya di komputer yang berbeda. Containerisasi membantu pengembang agar tidak perlu khawatir tentang pengaturan dan perbedaan di berbagai komputer.

## DevOps

DevOps adalah proses kerjasama yang dlakukan oleh developers atau tim pengembangan dengan tim operasi atau operations. Dalam proses pengembangan dan pengoperasian sebuah aplikasi, DevOps membuat prosesnya menjadi lebih baik, cepat, efisien dan stabil. Ketika developer membuat aplikasi, mereka bekerja sama dengan tim operasi untuk memastikan aplikasi tersebut dapat dijalankan dengan baik di komputer atau perangkat lain. Tim operasi membantu pengembang dengan memastikan aplikasi berjalan dengan aman dan tidak mengganggu aplikasi lain.

## Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata

Instagram adalah salah satu platform media sosial yang sangat populer dan digunakan oleh jutaan pengguna di seluruh dunia. DevOps, singkatan dari Development dan Operations, adalah praktik pengembangan perangkat lunak yang menggabungkan tim pengembang (development) dan tim operasi (operations) dalam sebuah siklus pengembangan yang terintegrasi untuk mencapai tujuan bisnis yang lebih baik. Berikut adalah contoh penerapan DevOps di Instagram:

1. Otomatisasi dan Continuous Integration/Continuous Deployment (CI/CD): Instagram menggunakan otomatisasi dalam proses pengembangan perangkat lunak mereka. Mereka melakukan otomatisasi pengujian, pembangunan (build), dan pelepasan (release) dengan menggunakan alat seperti Jenkins atau GitLab CI/CD. Hal ini memungkinkan mereka untuk mengotomatiskan proses pengembangan dan pelepasan perangkat lunak secara cepat dan konsisten.

2. Infrastruktur sebagai Kode (Infrastructure as Code): Instagram menggunakan pendekatan infrastruktur sebagai kode untuk mengelola dan menyebarkan infrastruktur mereka. Mereka menggunakan alat-alat seperti Terraform atau Ansible untuk mendefinisikan infrastruktur mereka sebagai kode yang dapat dikelola dan didokumentasikan secara terpusat. Ini memungkinkan mereka untuk mengatur infrastruktur secara efisien, konsisten, dan dapat direproduksi.

3. Manajemen Konfigurasi: Instagram menggunakan manajemen konfigurasi untuk memastikan konsistensi dan stabilitas lingkungan mereka. Mereka menggunakan alat seperti Puppet atau Chef untuk mengotomatiskan dan mengelola konfigurasi perangkat lunak dan sistem operasi di berbagai server mereka. Dengan manajemen konfigurasi, mereka dapat memastikan bahwa konfigurasi dan perubahan dapat diterapkan dengan cepat dan konsisten di seluruh infrastruktur mereka.

4. Monitoring dan Log: Instagram menggunakan alat monitoring seperti Prometheus atau Datadog untuk memantau kesehatan sistem, kinerja, dan tingkat pemakaian. Mereka juga mengumpulkan dan menganalisis log dari sistem dan aplikasi mereka menggunakan alat seperti ELK (Elasticsearch, Logstash, dan Kibana) stack. Dengan ini, mereka dapat mendeteksi masalah secara cepat, mengawasi performa aplikasi, dan melakukan analisis untuk pemeliharaan dan perbaikan yang lebih baik.

5. Kolaborasi Tim dan Kultur DevOps: Instagram menerapkan pendekatan kolaboratif dan budaya DevOps dalam organisasi mereka. Mereka memfasilitasi komunikasi dan kerjasama yang erat antara tim pengembangan dan tim operasi, memastikan pemahaman yang sama tentang tujuan bisnis dan tujuan teknis. Mereka juga mendorong adopsi praktik-praktik DevOps seperti Agile, iterasi cepat, dan pemecahan masalah berbasis data.

Melalui penerapan praktik DevOps, Instagram dapat mempercepat pengembangan dan pelepasan fitur baru, meningkatkan stabilitas dan kualitas perangkat lunak, serta memaksimalkan performa dan skalabilitas platform mereka. Ini memungkinkan mereka untuk memberikan pengalaman pengguna yang lebih baik dan menjaga keunggulan kompetitif di pasar media sosial yang serba cepat.


# 6.Mampu menjelaskan dan mendemonstrasikan web page / web service yang telah dapat diakses public dalam bentuk video Youtube (kumpulkan url video Youtube) 

> berikut link video penjelasan program yang saya buat:

>https://youtu.be/COZCdtqA1ZI


# 7. Mampu mempublikasikan Docker Image yang telah dibuat ke Docker Hub, untuk memudahkan penciptaan container sistem operasi di berbagai server pada distributed computing 

> berikut link Docker Hub program saya:

>https://hub.docker.com/repository/docker/abdullahamali21/nomor-darurat


