import streamlit as st

emergency_numbers = {
    "Polisi": "110",
    "Pemadam Kebakaran": "113",
    "Ambulans": "118",
    "SAR (Search and Rescue)": "115",
    "PLN (Gangguan Listrik)": "123",
    "Bencana Alam": "129",
    "Kejadian Kereta Api": "121",
    "Posko COVID-19": "119",
    "BPBD (Bencana Alam)": "112",
    "Rumah Sakit": "119",
    "Korlantas Polri": "107",
    "Tagana (Penanggulangan Bencana)": "111",
    "Satgas Pangan": "1500",
    "Konsulat": "1298",
    "BPJS Kesehatan": "1500400",
    "Telepon Umum": "108",
    "Pos Indonesia": "161",
    "PLN (Layanan Pelanggan)": "123",
    "Telkom (Layanan Pelanggan)": "147",
    "Airnav (Bandara)": "172",
    "KAI (Kereta Api)": "121",
    "Pengaduan Penyalahgunaan Narkoba": "198",
    "Kementerian Pariwisata": "1500622",
    "Kementerian Perhubungan": "1500345",
    "Kementerian Lingkungan Hidup dan Kehutanan": "112",
    "Kementerian Sosial": "1500299",
    "Kementerian Pertanian": "1500600",
    "Kementerian Kelautan dan Perikanan": "1500910"
}

def main():
    st.title("Daftar Panggilan Darurat di Indonesia")

    # Menampilkan pilihan panggilan darurat
    emergency_service = st.selectbox(
        "Pilih jenis panggilan darurat:",
        list(emergency_numbers.keys())
    )

    # Menampilkan nomor darurat yang dipilih
    if st.button("Tampilkan nomor darurat"):
        st.write(f"Nomor darurat {emergency_service}: {emergency_numbers[emergency_service]}")

if __name__ == "__main__":
    main()
